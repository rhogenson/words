#!/usr/bin/env python

import random

wordList = open("words.txt")
letterNumerators = [[0 for i in range(27)] for i in range(27)]
letterDenominators = [0 for i in range(27)]
wordBeginnings = [0 for i in range(27)]
wordsNum = 0

for line in wordList:
    line = line.lower()
    line = line[:-1]
    acceptable = True
    for letter in line:
        if (ord(letter) < 97 or ord(letter) > 122):
            acceptable = False

    if len(line) < 0:
        acceptable = False
    if acceptable:
        wordsNum += 1
        wordBeginnings[ord(line[0]) - 96] += 1
        for i in range(len(line) - 1):
            letterDenominators[ord(line[i]) - 96] += 1
            letterNumerators[ord(line[i]) - 96][ord(line[i + 1]) - 96] += 1
        letterDenominators[ord(line[-1]) - 96] += 1
        letterNumerators[ord(line[-1]) - 96][0] += 1


while True:
    newWord = ""
    j = 0
    number = random.SystemRandom().random() * wordsNum
    for i in range(len(wordBeginnings)):
        number -= wordBeginnings[i]
        if number < 0:
            newWord += (chr(i + 96))
            break

    newWordFinished = False
    while not newWordFinished:
        number = letterDenominators[ord(newWord[j]) - 96] * random.SystemRandom().random()
        for i in range(len(letterNumerators[ord(newWord[j]) - 96])):
            number -= letterNumerators[ord(newWord[j]) - 96][i]
            if number < 0:
                if i == 0:
                    newWordFinished = True
                    break
                j = j + 1
                newWord += (chr(i + 96))
                break

    print(newWord)
